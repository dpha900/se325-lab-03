package se325.lab03.concert.services;

import static se325.lab03.concert.services.SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT;

import java.net.URI;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javafx.scene.media.Media;
import se325.lab03.concert.common.Config;
import se325.lab03.concert.domain.Concert;;

/**
 * Class to implement a simple REST Web service for managing Concerts.
 * <p>
 * ConcertResource implements a WEB service with the following interface: - GET
 * <base-uri>/concerts/{id} Retrieves a Concert based on its unique id. The HTTP
 * response message has a status code of either 200 or 404, depending on whether
 * the specified Concert is found.
 * <p>
 * - GET <base-uri>/concerts?start&size Retrieves a collection of Concerts,
 * where the "start" query parameter identifies an index position, and "size"
 * represents the max number of successive Concerts to return. The HTTP response
 * message returns 200.
 * <p>
 * - POST <base-uri>/concerts Creates a new Concert. The HTTP post message
 * contains a representation of the Concert to be created. The HTTP Response
 * message returns a Location header with the URI of the new Concert and a
 * status code of 201.
 * <p>
 * - DELETE <base-uri>/concerts Deletes all Concerts, returning a status code of
 * 204.
 * <p>
 * Where a HTTP request message doesn't contain a cookie named clientId
 * (Config.CLIENT_COOKIE), the Web service generates a new cookie, whose value
 * is a randomly generated UUID. The Web service returns the new cookie as part
 * of the HTTP response message.
 */
@Path("/concerts")
public class ConcertResource {

	private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);

	private Map<Long, Concert> concertDB = new ConcurrentHashMap<>();
	private AtomicLong idCounter = new AtomicLong();

	@GET
	@Path("{id}")
	@Produces({APPLICATION_JAVA_SERIALIZED_OBJECT, MediaType.APPLICATION_JSON})
	public Response retrieveConcert(@PathParam("id") long id, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
		Concert found = concertDB.get(id);
		if (found == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		LOGGER.info("Retrived concert # " + id);
		return Response.ok(found).cookie(makeCookie(clientId)).build();
	}

	@GET
	@Produces({APPLICATION_JAVA_SERIALIZED_OBJECT, MediaType.APPLICATION_JSON})
	public Response retrieveConcerts(@QueryParam("start") long start, @QueryParam("size") int size, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
		ArrayList<Concert> concerts = new ArrayList<Concert>();
		LOGGER.info("REQUEST" + start + " : " + size);
		for (long i = start; i < start + size; i++) {
			Concert concert = concertDB.get(i);
			if (concert == null) {
				break;
			}
			concerts.add(concert);
		}

		GenericEntity<ArrayList<Concert>> entity = new GenericEntity<ArrayList<Concert>>(concerts) {};
		return Response.ok(entity).cookie(makeCookie(clientId)).build();
	}

	@POST
	@Consumes({APPLICATION_JAVA_SERIALIZED_OBJECT, MediaType.APPLICATION_JSON})
	public Response createConcert(Concert concert, @CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
		Concert newConcert = new Concert(idCounter.incrementAndGet(), concert.getTitle(), concert.getDate());
		concertDB.put(newConcert.getId(), newConcert);
		LOGGER.info("Created concert with id: " + newConcert.getId());
		return Response.created(URI.create("/concerts/" + newConcert.getId())).cookie(makeCookie(clientId)).build();
	}

	@DELETE
	public Response deleteAllConcerts(@CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
		concertDB.clear();
		idCounter = new AtomicLong();
		LOGGER.info("Deleted all concerts");
		return Response.noContent().cookie(makeCookie(clientId)).build();
	}

	/**
	 * Helper method that can be called from every service method to generate a
	 * NewCookie instance, if necessary, based on the clientId parameter.
	 *
	 * @param clientId
	 *            the Cookie whose name is Config.CLIENT_COOKIE, extracted from a
	 *            HTTP request message. This can be null if there was no cookie
	 *            named Config.CLIENT_COOKIE present in the HTTP request message.
	 * @return a NewCookie object, with a generated UUID value, if the clientId
	 *         parameter is null. If the clientId parameter is non-null (i.e. the
	 *         HTTP request message contained a cookie named Config.CLIENT_COOKIE),
	 *         this method returns null as there's no need to return a NewCookie in
	 *         the HTTP response message.
	 */
	private NewCookie makeCookie(@CookieParam(Config.CLIENT_COOKIE) Cookie clientId) {
		NewCookie newCookie = null;

		if (clientId == null) {
			newCookie = new NewCookie(Config.CLIENT_COOKIE, UUID.randomUUID().toString());
			LOGGER.info("Generated cookie: " + newCookie.getValue());
		}

		return newCookie;
	}
}

package se325.lab03.concert.domain;

public enum Genre {
    Pop, HipHop, RhythmAndBlues, Acappella, Metal, Rock
}